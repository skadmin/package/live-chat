<?php

declare(strict_types=1);

namespace Skadmin\LiveChat\Doctrine\LiveChatMessage;

use DateTimeInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\LiveChat\Doctrine\LiveChat\LiveChat;
use SkadminUtils\DoctrineTraits\Facade;

use function assert;

final class LiveChatMessageFacade extends Facade
{
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = LiveChatMessage::class;
    }

    public function create(LiveChat $liveChat, string $name, string $content, bool $isImportant = false): LiveChatMessage
    {
        $liveChatMessage = $this->get();
        $liveChatMessage->create($liveChat, $name, $content, $isImportant);

        $this->em->persist($liveChatMessage);
        $this->em->flush();

        return $liveChatMessage;
    }

    public function get(?int $id = null): LiveChatMessage
    {
        if ($id === null) {
            return new LiveChatMessage();
        }

        $liveChatMessage = parent::get($id);

        if ($liveChatMessage === null) {
            return new LiveChatMessage();
        }

        return $liveChatMessage;
    }

    /**
     * @return LiveChatMessage[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria);
    }

    /**
     * @return LiveChatMessageFacade[]
     */
    public function getForLiveChat(LiveChat $liveChat, ?DateTimeInterface $fromDate = null): array
    {
        $repository = $this->em->getRepository($this->table);
        assert($repository instanceof EntityRepository);

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('liveChat', $liveChat));

        if ($fromDate !== null) {
            $criteria->andWhere(Criteria::expr()->gt('createdAt', $fromDate));
        }

        return $repository->createQueryBuilder('a')
            ->orderBy('a.createdAt', 'DESC')
            ->addCriteria($criteria)
            ->getQuery()
            ->getResult();
    }
}
