<?php

declare(strict_types=1);

namespace Skadmin\LiveChat\Doctrine\LiveChatMessage;

use Doctrine\ORM\Mapping as ORM;
use Skadmin\LiveChat\Doctrine\LiveChat\LiveChat;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class LiveChatMessage
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Content;
    use Entity\Created;

    #[ORM\Column(options: ['default' => false])]
    private bool $isImportant = false;

    #[ORM\ManyToOne(targetEntity: LiveChat::class, inversedBy: 'messages')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private LiveChat $liveChat;

    public function create(LiveChat $liveChat, string $name, string $content, bool $isImportant): void
    {
        $this->liveChat    = $liveChat;
        $this->name        = $name;
        $this->content     = $content;
        $this->isImportant = $isImportant;
    }

    public function getLiveChat(): LiveChat
    {
        return $this->liveChat;
    }

    public function isImportant(): bool
    {
        return $this->isImportant;
    }
}
