<?php

declare(strict_types=1);

namespace Skadmin\LiveChat\Doctrine\LiveChat;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

final class LiveChatFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = LiveChat::class;
    }

    public function create(string $name, string $code): LiveChat
    {
        $liveChat = $this->get();
        $liveChat->create($name, $code);

        $this->em->persist($liveChat);
        $this->em->flush();

        return $liveChat;
    }

    public function get(?int $id = null): LiveChat
    {
        if ($id === null) {
            return new LiveChat();
        }

        $liveChat = parent::get($id);

        if ($liveChat === null) {
            return new LiveChat();
        }

        return $liveChat;
    }

    /**
     * @return LiveChat[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria);
    }

    public function findByWebalize(string $code, ?string $name = null): ?LiveChat
    {
        $criteria = ['code' => $code];

        $liveChat = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        if ($name === null || $liveChat !== null) {
            return $liveChat;
        }

        return $this->create($name, $code);
    }
}
