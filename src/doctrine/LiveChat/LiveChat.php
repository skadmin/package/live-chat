<?php

declare(strict_types=1);

namespace Skadmin\LiveChat\Doctrine\LiveChat;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\LiveChat\Doctrine\LiveChatMessage\LiveChatMessage;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class LiveChat
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Code;

    /** @var ArrayCollection|Collection|LiveChatMessage[] */
    #[ORM\OneToMany(targetEntity: LiveChatMessage::class, mappedBy: 'liveChat')]
    #[ORM\OrderBy(['createdAt' => 'DESC'])]
    private $messages;

    public function __construct()
    {
        $this->messages = new ArrayCollection();
    }

    public function create(string $name, string $code): void
    {
        $this->name = $name;
        $this->code = $code;
    }

    /**
     * @return ArrayCollection|Collection|LiveChatMessage[]
     */
    public function getMessages()
    {
        return $this->messages;
    }
}
