<?php

declare(strict_types=1);

namespace Skadmin\LiveChat;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'live-chat';
    public const DIR_IMAGE = 'live-chat';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-comments']),
            'items'   => ['overview'],
        ]);
    }
}
