<?php

declare(strict_types=1);

namespace Skadmin\LiveChat\Components\Admin;

use App\Model\System\APackageControl;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\LiveChat\BaseControl;
use Skadmin\LiveChat\Doctrine\LiveChat\LiveChat;
use Skadmin\LiveChat\Doctrine\LiveChat\LiveChatFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class Overview extends GridControl
{
    use APackageControl;

    private LiveChatFacade $facade;
    private LoaderFactory  $webLoader;

    public function __construct(LiveChatFacade $facade, Translator $translator, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade    = $facade;
        $this->webLoader = $webLoader;
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'live-chat.overview.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()
            ->orderBy('a.id', 'DESC'));

        // COLUMNS
        $grid->addColumnText('name', 'grid.live-chat.overview.name')
            ->setRenderer(function (LiveChat $liveChat): Html {
                $webalize = Html::el('code', ['class' => 'text-muted small'])->setText($liveChat->getCode());

                $link = $this->getPresenter()->link('Component:default', [
                    'package' => new BaseControl(),
                    'render'  => 'chat',
                    'id'      => $liveChat->getId(),
                ]);

                $href = Html::el('a', [
                    'href'  => $link,
                    'class' => 'font-weight-bold',
                ])->setText($liveChat->getName());

                $result = new Html();
                $result->addHtml($href)
                    ->addHtml('<br>')
                    ->addHtml($webalize);

                return $result;
            });
        $grid->addColumnText('items-count', 'grid.live-chat.overview.items-count')
            ->setRenderer(static function (LiveChat $liveChat): int {
                return $liveChat->getMessages()->count();
            })->setAlign('center');

        // FILTER
        $grid->addFilterText('name', 'grid.live-chat.overview.name', ['name', 'code']);

        // ACTION
        $grid->addAction('chat', 'grid.live-chat.overview.action.chat', 'Component:default', ['id' => 'id'])
            ->addParameters([
                'package' => new BaseControl(),
                'render'  => 'chat',
            ])->setIcon('comments')
            ->setClass('btn btn-xs btn-outline-primary');

        return $grid;
    }
}
