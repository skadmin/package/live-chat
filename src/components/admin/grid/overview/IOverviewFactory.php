<?php

declare(strict_types=1);

namespace Skadmin\LiveChat\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
