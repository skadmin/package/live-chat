<?php

declare(strict_types=1);

namespace Skadmin\LiveChat\Components\Admin;

use App\Model\System\APackageControl;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Skadmin\LiveChat\BaseControl;
use Skadmin\LiveChat\Components\Front\Chat as ChatFront;
use Skadmin\LiveChat\Components\Front\IChatFactory as IChatFrontFactory;
use Skadmin\LiveChat\Doctrine\LiveChat\LiveChat;
use Skadmin\LiveChat\Doctrine\LiveChat\LiveChatFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Role\Doctrine\Role\Role;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use WebLoader\Nette\LoaderFactory;

use function implode;
use function sprintf;

class Chat extends GridControl
{
    use APackageControl;

    private LoaderFactory $webLoader;
    private LiveChat      $liveChat;
    private ChatFront     $chat;

    public function __construct(int $id, LiveChatFacade $facade, IChatFrontFactory $iChatFrontFactory, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->liveChat = $facade->get($id);

        $roles            = Arrays::map($this->loggedUser->getIdentity()->getRolesObject(), static fn (Role $role): string => $role->getName());
        $userNameWithRole = sprintf('%s [%s]', $this->loggedUser->getIdentity()->getFullName(), implode(', ', $roles));

        $this->chat = $iChatFrontFactory->create($this->liveChat, $userNameWithRole, true);
        $this->chat->setDrawBox(false);
    }

    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/chat.latte');

        $template->linkBack = $this->getPresenter()
            ->link('Component:default', [
                'package' => new BaseControl(),
                'render'  => 'overview',
            ]);

        $template->render();
    }

    public function getTitle(): SimpleTranslation
    {
        return new SimpleTranslation('live-chat.chat.title - %s', $this->liveChat->getName());
    }

    public function createComponentChat(): ChatFront
    {
        return $this->chat;
    }
}
