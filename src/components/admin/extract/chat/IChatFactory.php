<?php

declare(strict_types=1);

namespace Skadmin\LiveChat\Components\Admin;

interface IChatFactory
{
    public function create(int $id): Chat;
}
