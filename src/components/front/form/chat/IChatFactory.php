<?php

declare(strict_types=1);

namespace Skadmin\LiveChat\Components\Front;

use Skadmin\LiveChat\Doctrine\LiveChat\LiveChat;

interface IChatFactory
{
    public function create(LiveChat $liveChat, string $name, bool $isImportant = false, bool $autoRedraw = true): Chat;
}
