<?php

declare(strict_types=1);

namespace Skadmin\LiveChat\Components\Front;

use App\Model\System\APackageControl;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Skadmin\LiveChat\Doctrine\LiveChat\LiveChat;
use Skadmin\LiveChat\Doctrine\LiveChatMessage\LiveChatMessageFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;

use function is_bool;

class Chat extends FormWithUserControl
{
    use APackageControl;

    private LiveChatMessageFacade $facade;
    private LiveChat              $liveChat;
    private string                $name;
    private bool                  $isImportant;
    private bool                  $autoRedraw;
    private ?DateTime             $datetime = null;

    public function __construct(LiveChat $liveChat, string $name, bool $isImportant, bool $autoRedraw, LiveChatMessageFacade $facade, Translator $translator, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade = $facade;

        $this->liveChat    = $liveChat;
        $this->name        = $name;
        $this->isImportant = $isImportant;
        $this->autoRedraw  = $autoRedraw;
    }

    public function getTitle(): string
    {
        return 'form.live-chat.front.chat.title';
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $chat = $this->facade->create($this->liveChat, $this->name, $values->content, $this->isImportant);

        $form->reset();
        $this->getPresenter()->redrawControl('snipModal', false);
        $this->redrawControl('snipForm');

        $this->onRedraw();
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/chat.latte'));

        $template->drawBox    = $this->drawBox;
        $template->messages   = $this->facade->getForLiveChat($this->liveChat, $this->datetime);
        $template->autoRedraw = $this->autoRedraw;

        $template->render();
    }

    public function handleInvalidateRedraw(): void
    {
        $this->onRedraw();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        $form->addTextArea('content', 'form.live-chat.front.chat.content', null, 5)
            ->setRequired('form.live-chat.front.chat.content.req');

        // BUTTON
        $form->addSubmit('send', 'form.live-chat.front.chat.send');

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function handleLoadNewMessage(?string $datetime = null): void
    {
        if ($datetime !== null) {
            $date           = DateTime::createFromFormat('YmdHis', $datetime);
            $this->datetime = is_bool($date) ? new DateTime() : $date;
        } else {
            $this->datetime = new DateTime();
        }

        $this->getPresenter()->redrawControl('snipModal', false);
        $this->redrawControl('snipChatMessages');
    }
}
