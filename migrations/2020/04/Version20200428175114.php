<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200428175114 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translationsAdmin = [
            ['original' => 'live-chat.overview', 'hash' => 'd408141242f9e0303a650821cb9fa06d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Live chat', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.live-chat.title', 'hash' => '68878d718d48e263e9d58e096c4bdbf5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Live chat', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.live-chat.description', 'hash' => '7623d98cd33ddb23f50881cfe061bcfa', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Umožňuje reagovat na live chat jako admin', 'plural1' => '', 'plural2' => ''],
            ['original' => 'live-chat.overview.title', 'hash' => 'fc74633d89148ab01277943277b60c82', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Live chat|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.live-chat.overview.name', 'hash' => 'f5f93c6ce5b1d4ddb1cb4271b25f8839', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název chatu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.live-chat.overview.items-count', 'hash' => '66d7264fb013f224271e0978a41f6568', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet zpráv', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.live-chat.overview.action.chat', 'hash' => 'bf3725e3ad97c63061ffd58e4ece1b31', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vstoupit do chatu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.live-chat.front.chat.content', 'hash' => '5a15a377332fad2d4c827a1663d786b2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpráva', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.live-chat.front.chat.content.req', 'hash' => '0f417f39af12dc244f2fe1f0510c3bb5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím zprávu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.live-chat.front.chat.send', 'hash' => 'ff68b3b451c5084fdd708cf8aaee16ee', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odeslat', 'plural1' => '', 'plural2' => ''],
            ['original' => 'live-chat.chat.title - %s', 'hash' => 'd43a3bfad16e66e82937e4ed4d60ed89', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Live chat', 'plural1' => '', 'plural2' => ''],
            ['original' => 'live-chat.chat.back', 'hash' => '5d2587bbdd1c88a9ca8f79cf06cffa43', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět na přehled', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translationsAdmin as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }

        $translationsFront = [
            ['original' => 'form.live-chat.front.chat.content', 'hash' => '0e15efb09a1d7774da3a1a4d5bd1f9d5', 'module' => 'front', 'language_id' => 1, 'singular' => 'Zpráva', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.live-chat.front.chat.content.req', 'hash' => '2156fb6a98e0a0d03b08c94c9b32d762', 'module' => 'front', 'language_id' => 1, 'singular' => 'Zadejte prosím zprávu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.live-chat.front.chat.send', 'hash' => '5943f2370d223ad0390efb54338eef9f', 'module' => 'front', 'language_id' => 1, 'singular' => 'Odeslat', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translationsFront as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
    }
}
